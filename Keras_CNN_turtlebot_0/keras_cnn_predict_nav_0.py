import math
import random
import time
import numpy as np
import matplotlib.pyplot as plt
import cv2
import os
import glob

from keras.models import Sequential
from keras.layers import Input
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.optimizers import SGD, Adam
from keras.utils import np_utils
from keras.utils.vis_utils import model_to_dot

# Raw training data file name
input_data_file = "testing_data.npy"

# Number of epoch
NP_EPOCH = 200 
NP_BATCH_SIZE = 64 

IMG_HEIGHT = 120
IMG_WIDTH = 160
IMG_CHANNEL = 1

#ACT_METHOD = "sigmoid"
ACT_METHOD = "relu"
VEL_OUTPUT_ACT_METHOD ="linear"
LOSS = "mse"  # "mse" for regression, "categorical_crossentropy" for classification
OPTIMIZER = "rmsprop"

LOAD_WEIGHT = True #True 
SAVE_WEIGHT = False #True


def load_data(filename):
  print("loading data file")
  if(os.path.isfile(filename)):
    print("File exist, loading")
    input_data = np.load(filename)
  else:
    print("File does NOT exist.")
    exit()

  print("lenth of original input_data={}".format(len(input_data)))

  img_data = np.array([data[0] for data in input_data])
  print(img_data.shape)
  img_data = img_data.reshape(-1, IMG_HEIGHT, IMG_WIDTH, IMG_CHANNEL)
  print(img_data.shape)
  cmd_data = np.array([data[1] for data in input_data])
  print(cmd_data.shape)
  
  return img_data, cmd_data



# Load data from each label-directory
print("== Reading training and test data ==") 
(X_predict, Y_truth) = load_data(input_data_file)

print(X_predict.shape)
print(Y_truth.shape)
print(type(X_predict))
print(type(Y_truth))


# Draw samples of input images
#for i in range(80, 140, 1):
#    cv2.imshow("X_train[%d]" % i , X_train[i])
#    cv2.waitKey(250)
#    cv2.destroyAllWindows()

data_img_size = IMG_HEIGHT * IMG_WIDTH * IMG_CHANNEL


# build simple 2-layer convoluion network model
print("Activation method = %s" % ACT_METHOD)
print("Output activation method = %s (NO EFFECT ATM)" % VEL_OUTPUT_ACT_METHOD)
print("Loss = %s" % LOSS)
print("Optimizer = %s" % OPTIMIZER)

td_inputs = Input(shape=(IMG_HEIGHT, IMG_WIDTH, IMG_CHANNEL))
conv2d_0_outputs = Convolution2D(32, 11, 11, border_mode='same', activation='relu') (td_inputs)
pooling_0_outputs = MaxPooling2D(pool_size=(3, 3)) (conv2d_0_outputs)
normalized_0_outputs = BatchNormalization() (pooling_0_outputs)

conv2d_1_outputs = Convolution2D(64, 5, 5, activation='relu') (normalized_0_outputs)
pooling_1_outputs = MaxPooling2D(pool_size=(2, 2)) (conv2d_1_outputs)
normalized_1_outputs = BatchNormalization() (pooling_1_outputs)

conv2d_2_outputs = Convolution2D(96, 3, 3, activation='relu') (normalized_1_outputs)

conv2d_3_outputs = Convolution2D(96, 3, 3, activation='relu') (conv2d_2_outputs)

conv2d_4_outputs = Convolution2D(64, 3, 3, activation='relu') (conv2d_3_outputs)
pooling_4_outputs = MaxPooling2D(pool_size=(2, 2)) (conv2d_4_outputs)
normalized_4_outputs = BatchNormalization() (pooling_4_outputs)

flatten_5_outputs = Flatten() (normalized_4_outputs)

dense_6_outputs = Dense(1024, activation='relu') (flatten_5_outputs)

dense_7_outputs = Dense(1024, activation='relu') (dense_6_outputs)

linear_vel_output = Dense(1, name='linear_vel_output') (dense_7_outputs)
angular_vel_output = Dense(1, name='angular_vel_output') (dense_7_outputs)

model = Model(input=td_inputs, output=[linear_vel_output, angular_vel_output])

if LOAD_WEIGHT:
    print("loading weights")
    model.load_weights("model_weights.h5", by_name=True)

#compile model
model.compile(loss = LOSS, optimizer = OPTIMIZER)
print("Compiling model")

print("Prediction Testing")
Y_predict = model.predict(X_predict)
print("End of Prediction Testing")

print(Y_truth[0])
Y_output = np.array([data for data in Y_predict])
print(Y_output.shape)
print("Reshape output")
Y_output = Y_output.reshape(2, -1)
print(Y_output.shape)
print(Y_output)
print("Transpose output")
Y_output = Y_output.T
print(Y_output.shape)
print(Y_output)

print("Y_truth:")
print(Y_truth)
print("Y_output:")
print(Y_output)
#print(Y_output[:,0])

print("Activation method = %s" % ACT_METHOD)
print("Output activation method = %s" % VEL_OUTPUT_ACT_METHOD)
print("Loss = %s" % LOSS)
print("= test end =")

