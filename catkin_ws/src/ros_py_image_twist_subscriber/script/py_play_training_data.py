#!/usr/bin/env python
# Python Image Subscriber Node
# This node was modified with reference to imcmahon's reply on
# http://answers.ros.org/question/210294/ros-python-save-snapshot-from-camera/
# Basic idea is convert from ROS Image -> CvBridge Converter -> OpenCV
# Note you'd still need the CMakeList.txt and package.xml
# Reference: 
#  http://answers.ros.org/question/210294/ros-python-save-snapshot-from-camera/
#  http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_gui/py_image_display/py_image_display.html

import numpy as np                # numpy
import cv2                        # OpenCV2
import sys 
import os

linear_x = 0.0
angular_z = 0.0

filename = "training_data.npy"

training_data = []

def show(filename):
  print("Showing")
  if(os.path.isfile(filename)):
    print("File exist, loading")
    training_data = np.load(filename)
  else:
    print("File does NOT exist.")
    exit()

  for data in training_data:
    img = data[0]
    cmd = data[1]
    cv2.imshow("test", img)
    print("linear.x={0:2.2f}".format(cmd[0]) + "  angular.z={0:2.2f}".format(cmd[1]))
    if cv2.waitKey(50) & 0xFF == ord('q'):
      cv2.destroyAllWindows()
      break      

if __name__ == '__main__':
#    check_training_data_file(filename)
    show(filename) 
