#!/usr/bin/env python
# Python Image Subscriber Node
# This node was modified with reference to imcmahon's reply on
# http://answers.ros.org/question/210294/ros-python-save-snapshot-from-camera/
# Basic idea is convert from ROS Image -> CvBridge Converter -> OpenCV
# Note you'd still need the CMakeList.txt and package.xml
# Reference: 
#  http://answers.ros.org/question/210294/ros-python-save-snapshot-from-camera/
#  http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_gui/py_image_display/py_image_display.html

import numpy as np                # numpy
from collections import Counter
from random import shuffle
import pandas as pd
import cv2                        # OpenCV2
import sys 
import os


linear_x = 0.0
angular_z = 0.0

filename = "training_data.npy"

training_data = []

def show(filename):
  print("Showing")
  if(os.path.isfile(filename)):
    print("File exist, loading")
    training_data = np.load(filename)
  else:
    print("File does NOT exist.")
    exit()

  for data in training_data:
    img = data[0]
    cmd = data[1]
    cv2.imshow("test", img)
    print("linear.x={0:2.2f}".format(cmd[0]) + "  angular.z={0:2.2f}".format(cmd[1]))
    if cv2.waitKey(30) & 0xFF == ord('q'):
      cv2.destroyAllWindows()
      break      

def pandas_count(filename):
  print("Showing")
  if(os.path.isfile(filename)):
    print("File exist, loading")
    training_data = np.load(filename)
  else:
    print("File does NOT exist.")
    exit()

  df = pd.DataFrame(training_data)
  print(df.head())
  print(Counter(df[1].apply(str)))

def count_shuffle_blance(filename):
  print("Showing")
  if(os.path.isfile(filename)):
    print("File exist, loading")
    training_data = np.load(filename)
  else:
    print("File does NOT exist.")
    exit()

  shuffle(training_data)

  for data in training_data:
    img = data[0]
    cmd = data[1]
    cmd = np.around(cmd,decimals = 2)
    data[1] = cmd
    #print(cmd)
    
  df = pd.DataFrame(training_data)
  print(df.head())
  print(Counter(df[1].apply(str)))


  forward_032_000 = []
  forward_030_000 = []
  forward_028_000 = []
  forward_026_000 = []
  forward_024_000 = []
  turn_000_060 = []
  turn_000_050 = []
  turn_000_040 = []
  turn_000_030 = []
  turn_000_020 = []
  turn_000_010 = []
  move_028_020 = []
  move_030_010 = []
  idle_000_000 = []

  others = []

  for data in training_data:
    img = data[0]
    cmd = data[1]
    
    if ((cmd[0] == 0.32) and (cmd[1]== 0.0)):
        forward_032_000.append([img, cmd])
    elif ((cmd[0] == 0.30) and (cmd[1]== 0.0)):
        forward_030_000.append([img, cmd])
    elif ((cmd[0] == 0.28) and (cmd[1]== 0.0)):
        forward_028_000.append([img, cmd])
    elif ((cmd[0] == 0.26) and (cmd[1]== 0.0)):
        forward_026_000.append([img, cmd])
    elif ((cmd[0] == 0.24) and (cmd[1]== 0.0)):
        forward_024_000.append([img, cmd])
    elif ((cmd[0] == 0.00) and (cmd[1]== 0.6)):
        turn_000_060.append([img, cmd])
    elif ((cmd[0] == 0.00) and (cmd[1]== 0.5)):
        turn_000_050.append([img, cmd])
    elif ((cmd[0] == 0.00) and (cmd[1]== 0.4)):
        turn_000_040.append([img, cmd])
    elif ((cmd[0] == 0.00) and (cmd[1]== 0.3)):
        turn_000_030.append([img, cmd])
    elif ((cmd[0] == 0.00) and (cmd[1]== 0.2)):
        turn_000_020.append([img, cmd])
    elif ((cmd[0] == 0.00) and (cmd[1]== 0.1)):
        turn_000_010.append([img, cmd])
    elif ((cmd[0] == 0.28) and (cmd[1]== 0.2)):
        move_028_020.append([img, cmd])
    elif ((cmd[0] == 0.30) and (cmd[1]== 0.1)):
        move_030_010.append([img, cmd])
    elif ((cmd[0] == 0.0) and (cmd[1]== 0.0)):
        idle_000_000.append([img, cmd])
    else:
        others.append([img, cmd])

  forward_032_000 = forward_032_000[:1200]
  forward_030_000 = forward_030_000[:1200]
  forward_028_000 = forward_028_000[:1200]
  forward_026_000 = forward_026_000[:1200]
  forward_024_000 = forward_024_000[:1200]
  turn_000_060 = turn_000_060[:1000]
  turn_000_050 = turn_000_050[:1000]
  turn_000_040 = turn_000_040[:1000]
  turn_000_030 = turn_000_030[:1000]
  turn_000_020 = turn_000_020[:1000]
  turn_000_010 = turn_000_010[:1000]
  move_028_020 = move_028_020[:1000]
  move_030_010 = move_030_010[:1000]
  idle_000_000 = idle_000_000[:1200]

  balanced_training_data = others + forward_032_000 + forward_030_000 + forward_028_000 + forward_026_000 + forward_024_000 + turn_000_060 + turn_000_050 +turn_000_040 + turn_000_030 + turn_000_020 + turn_000_010 +idle_000_000

  shuffle(balanced_training_data)
  
  
  for data in balanced_training_data:
    img = data[0]
    cmd = data[1]
#    print(cmd)
 
  print("lenth of original training_data={}".format(len(training_data)))
  print("lenth of balanced_training_data={}".format(len(balanced_training_data)))

  np.save('balanced_training_data.npy', balanced_training_data)


if __name__ == '__main__':
#    check_training_data_file(filename)
    #show(filename)
    count_shuffle_blance(filename) 
